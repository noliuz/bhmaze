package com.bhahusut.bhmaze;

import java.util.LinkedList;
import java.util.List;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class MainActivity extends BaseGameActivity {
	final int mCameraWidth = 1280;
	final int mCameraHeight = 768;
	
	LoginScene mainScene;
	RoomSelectScene roomSelectScene;
	PlayScene playScene; 
	
	Engine eg;
	MainActivity act;
	VertexBufferObjectManager vbo;
	Camera cmr;
	public RemoteCommand rc;
	
	@Override
	public EngineOptions onCreateEngineOptions() {
		Camera mCamera = new Camera(0, 0, mCameraWidth, mCameraHeight);

		final EngineOptions engineOptions = new EngineOptions(true,
				ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(
						mCameraWidth, mCameraHeight), mCamera);
		engineOptions.getAudioOptions().setNeedsSound(true);
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		return engineOptions;

	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws Exception {
		
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws Exception {
		// TODO Auto-generated method stub
		eg = mEngine;
		act = this;
		vbo = eg.getVertexBufferObjectManager();
		cmr = eg.getCamera();
		rc = new RemoteCommand();
								
		
		//SceneManager sm = new SceneManager();
		//mainScene = new LoginScene(eg,act,vbo,cmr);
		//roomSelectScene = new RoomSelectScene(eg,act,vbo,cmr);
		playScene = new PlayScene(eg,act,vbo,cmr);
		
		pOnCreateSceneCallback.onCreateSceneFinished(playScene);
	}

	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
		// TODO Auto-generated method stub
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
	
}
