package com.bhahusut.bhmaze;

import java.util.ArrayList;
import java.util.Iterator;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.bhahusut.bhmaze.Data.Mak;
import com.bhahusut.bhmaze.Data.Point;

public class PlayScene extends BaseScene {
	BitmapTextureAtlas bgT, horT,verT,dotT,boxNT,boxWT,boxET,boxST,playerT,keypadT;
	ITextureRegion bgTR, horTR,verTR,dotTR,boxNTR,boxWTR,boxETR,boxSTR,playerTR,keypadTR;
	MazeGenerator mg;
	int scale = 4;
	int mazeX = 200,mazeY = 50;
	int keypadX = 50,keypadY = 500;
	ArrayList<Sprite> mazeSpAL = new ArrayList<Sprite>();
	int playerX=0,playerY=0;
	int playerDA[][];
	PlayScene sc;
	Sprite playerSp = null;
	
	PlayScene(Engine _eg, MainActivity _act, VertexBufferObjectManager _vbo,
			Camera _camera) {
		super(_eg, _act, _vbo, _camera);
		// TODO Auto-generated constructor stub
		this.createScene();
		sc = this;
	}

	@Override
	public void createScene() {
		// TODO Auto-generated method stub
		Log.e("RoomSelectScene","creating scene...");
		
		this.loadGFX();
		this.loadFont();
		
		mg = new MazeGenerator(10,10);		
		playerDA = new int[mg.x][mg.y];
		playerDA[playerX][playerY] = 1; //start player position
		
		drawMaze();
		drawThings();
		drawPlayer();
		
		this.createOnTouchPlayScene();
				
	}		
	
	public boolean chkWalkCollid(int px,int py,int dir) {
		if (dir == 1) {//up
			if ((mg.maze[px][py] & 1) == 0) {
				Log.e("collid","up");
				return true;
			}
				
		} else if (dir == 2) {//down
			if ((mg.maze[px][py] & 2) == 0) {
				Log.e("collid","down");
				return true;			
			}
		} else if (dir == 3) {//left
			if ((mg.maze[px][py] & 8) == 0) {
				Log.e("collid","left");
				return true;			
			}
		} else if (dir == 4) {//right
			if ((mg.maze[px][py] & 4) == 0) {
				Log.e("collid","right");
				return true;			
			}
		}
		
		return false;
	}
	
	public boolean chkPlayerSpriteCollid(int px,int py,int dir) {
		if (dir == 1) {//up
			for (Sprite sp : mazeSpAL) {
				sp.collidesWith(sp);
			}
			
		}
		return false;
	}
	
	void drawThings() {
		Sprite keypadS = new Sprite(keypadX,keypadY,keypadTR,vbo);
		this.attachChild(keypadS);
		
	}
	
	void createOnTouchPlayScene() {		
		
		// touching				
		this.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {								
				
				if (pSceneTouchEvent.isActionUp()) {										
					float tx = pSceneTouchEvent.getX();
					float ty = pSceneTouchEvent.getY();		
					
					if (tx >= keypadX+68 && tx <= keypadX+130 && ty >= keypadY+10 && ty <= keypadY+65) {//up
						if (!sc.chkWalkCollid(playerX, playerY, 1)) {
							playerY--;							
							
						}
						//Log.e("keypad","up");
					} else if (tx >= keypadX+68 && tx <= keypadX+130 && ty >= keypadY+145 && ty <= keypadY+196) {//down
						if (!sc.chkWalkCollid(playerX, playerY, 2)) {
							playerY++;							
						}
						
						//Log.e("keypad","down");
					} else if (tx >= keypadX+13 && tx <= keypadX+70 && ty >= keypadY+80 && ty <= keypadY+135) {//left
						if (!sc.chkWalkCollid(playerX, playerY, 3)) {
							playerX--;							
						}
							
						//Log.e("keypad","left");
					} else if (tx >= keypadX+126 && tx <= keypadX+185 && ty >= keypadY+80 && ty <= keypadY+135) {//right
						if (!sc.chkWalkCollid(playerX, playerY, 4)) {
							playerX++;							
						}
						//Log.e("keypad","right");
					}
					
					sc.drawPlayer();
					
				}
					
					
				
				return true;
			}
		});
	}				
	
	@Override
	public void loadGFX() {
		// TODO Auto-generated method stub
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");						
		
		keypadT = new BitmapTextureAtlas(eg.getTextureManager(),
				200, 200, TextureOptions.BILINEAR);						
		keypadTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						keypadT, act, "keypad.png",0,0,1,1);
		keypadT.load();
		
		verT = new BitmapTextureAtlas(eg.getTextureManager(),
				2, 10, TextureOptions.BILINEAR);						
		verTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						verT, act, "ver.png",0,0,1,1);
		verT.load();
		
		horT = new BitmapTextureAtlas(eg.getTextureManager(),
				10, 1, TextureOptions.BILINEAR);						
		horTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						horT, act, "hor.png",0,0,1,1);
		horT.load();
		
		dotT = new BitmapTextureAtlas(eg.getTextureManager(),
				10, 2, TextureOptions.BILINEAR);						
		dotTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						dotT, act, "dot.jpg",0,0,1,1);
		dotT.load();
		
		boxNT = new BitmapTextureAtlas(eg.getTextureManager(),
				10, 10, TextureOptions.BILINEAR);						
		boxNTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						boxNT, act, "boxN.jpg",0,0,1,1);
		boxNT.load();
		
		boxWT = new BitmapTextureAtlas(eg.getTextureManager(),
				10, 10, TextureOptions.BILINEAR);						
		boxWTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						boxWT, act, "boxW.jpg",0,0,1,1);
		boxWT.load();
		
		boxET = new BitmapTextureAtlas(eg.getTextureManager(),
				10, 10, TextureOptions.BILINEAR);						
		boxETR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						boxET, act, "boxE.jpg",0,0,1,1);
		boxET.load();
		
		boxST = new BitmapTextureAtlas(eg.getTextureManager(),
				10, 10, TextureOptions.BILINEAR);						
		boxSTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						boxST, act, "boxS.jpg",0,0,1,1);
		boxST.load();
		
		playerT = new BitmapTextureAtlas(eg.getTextureManager(),
				8, 8, TextureOptions.BILINEAR);						
		playerTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						playerT, act, "player.png",0,0,1,1);
		playerT.load();
		
	}

	public void drawPlayer() {
		
		eg.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (playerSp != null) {
					playerSp.detachSelf();
					playerSp.dispose();
				}
				
				playerSp = new Sprite(playerX*10*scale+2+mazeX,playerY*10*scale+2+mazeY,playerTR,vbo);
				playerSp.setScale(scale);
				sc.attachChild(playerSp);					
			}			
		});
	}
	
	public void loadFont() {
		FontFactory.setAssetBasePath("font/");
        final ITexture fontTexture = new BitmapTextureAtlas(
					act.getTextureManager(), 1024, 1024,
					TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        /*
		textF = FontFactory.createFromAsset(act.getFontManager(), fontTexture,
					act.getAssets(), "TAHOMA.TTF", 40, true,
					Color.WHITE.getARGBPackedInt());
		textF.load();*/
	}
	
	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}
	
	public void drawMaze() {		
		//fill top		
		for (int i=0;i<mg.x;i++) {
			mg.maze[i][0] = mg.maze[i][0] & 14; 
		}		
		//fill bottom
		for (int i=0;i<mg.x;i++) {
			mg.maze[i][mg.y-1] = mg.maze[i][mg.y-1] & 13; 
		}
		
		
		for (int i = 0; i < mg.y; i++) {			 
			for (int j = 0; j < mg.x; j++) {
				// draw the north edge
				if ((mg.maze[j][i] & 1) == 0) {
					Sprite sp = new Sprite(mazeX+j*10*scale,mazeY+i*10*scale,boxNTR,vbo);
					sp.setScale(scale);
					this.attachChild(sp);
					mazeSpAL.add(sp);
				} 								
				
				// draw west edge
				if ((mg.maze[j][i] & 8) == 0) {
					Sprite sp = new Sprite(mazeX+j*10*scale,mazeY+i*10*scale,boxWTR,vbo);
					sp.setScale(scale);
					this.attachChild(sp);
					mazeSpAL.add(sp);
				}												
				
				// draw bottom edge
				if ((mg.maze[j][i] & 2) == 0) {
					Sprite sp = new Sprite(mazeX+j*10*scale,mazeY+i*10*scale,boxSTR,vbo);
					sp.setScale(scale);
					this.attachChild(sp);
					mazeSpAL.add(sp);
				}
				
				// draw east edge
				if ((mg.maze[j][i] & 4) == 0) {
					Sprite sp = new Sprite(mazeX+j*10*scale,mazeY+i*10*scale,boxETR,vbo);
					sp.setScale(scale);
					this.attachChild(sp);
					mazeSpAL.add(sp);
				}
				
			}			
		}		
				
		/*
		//draw top
		for (int i=0;i<mg.x;i++) {
			Sprite sp = new Sprite(mazeX+i*10*scale,mazeY-(5*(scale-1)),horTR,vbo);
			sp.setScale(scale);
			this.attachChild(sp);
			mazeSpAL.add(sp);
		}
		
		//draw bottom
		for (int i=0;i<mg.x;i++) {
			Sprite sp = new Sprite(mazeX+i*10*scale,mazeY+mg.y*10*scale,boxNTR,vbo);
			sp.setScale(scale);
			this.attachChild(sp);
			mazeSpAL.add(sp);
		}
		//draw right wall
		for (int i=0;i<mg.y;i++) {
			Sprite sp = new Sprite(mazeX+mg.x*10*scale,mazeY+i*10*scale,boxWTR,vbo);
			sp.setScale(scale);
			this.attachChild(sp);
			mazeSpAL.add(sp);
		}*/
	}
	
}
