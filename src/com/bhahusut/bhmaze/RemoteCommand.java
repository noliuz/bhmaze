package com.bhahusut.bhmaze;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class RemoteCommand {
	String hostName = "http://192.168.1.22/mh_server/public/";
	
	public String sendCommand(String cmd,List<NameValuePair> pairl) {
		HttpClient httpClient = new DefaultHttpClient();
		String paramString;
		if (pairl == null) 
			paramString = "";
		else 
			paramString = URLEncodedUtils.format(pairl,"utf-8");
		
		String url = hostName+cmd+"?"+paramString;
		
		Log.e("url",url);
		
		HttpGet req = new HttpGet(url);	    	   
	    String resString = "xx";
		BufferedReader in = null;
	    
	    try {
	    	HttpResponse res = httpClient.execute(req);
	    	in = new BufferedReader(new InputStreamReader(
	                   res.getEntity().getContent()));
	    	resString = in.readLine();
	    } catch (Exception e) {
	    	Log.e("remoteCommand",e.getMessage());
	    }

		return resString;
	}
	
	public String pollCommand(String cmd,List<NameValuePair> pairl) {
		HttpClient httpClient = new DefaultHttpClient();
		String paramString;
		if (pairl == null) 
			paramString = "";
		else 
			paramString = URLEncodedUtils.format(pairl,"utf-8");
		
		String url = hostName+cmd+"?"+paramString;
		
		//Log.e("url",url);
		
		HttpGet req = new HttpGet(url);	    	   
	    String resString = "xx";
		BufferedReader in = null;
	    
	    try {
	    	HttpResponse res = httpClient.execute(req);
	    	in = new BufferedReader(new InputStreamReader(
	                   res.getEntity().getContent()));
	    	resString = in.readLine();
	    } catch (Exception e) {
	    	Log.e("pollCommand",e.getMessage());
	    }

		return resString;
	}
	
	public class PollTask extends AsyncTask<String, Void,String> {
		ArrayList<NameValuePair> nvpAL = null;
		String cmd="";
		
		public PollTask(String _cmd,ArrayList<NameValuePair> _nvpAL) {
			nvpAL = _nvpAL;
			cmd = _cmd;			
		}
		
		
		@Override
	    protected String doInBackground(String... urls) {
			HttpClient httpClient = new DefaultHttpClient();
			String paramString;
			if (nvpAL == null) 
				paramString = "";
			else 
				paramString = URLEncodedUtils.format(nvpAL,"utf-8");
			
			String url = hostName+cmd+"?"+paramString;
			
			//Log.e("url",url);
			
			HttpGet req = new HttpGet(url);	    	   
		    String resString = "xx";
			BufferedReader in = null;
		    
		    try {
		    	HttpResponse res = httpClient.execute(req);
		    	in = new BufferedReader(new InputStreamReader(
		                   res.getEntity().getContent()));
		    	resString = in.readLine();
		    } catch (Exception e) {
		    	Log.e("pollTask",e.getMessage());
		    }

			return resString;	      		
	    }
	}
	
	/*
	public class bgSendCommandTask extends AsyncTask<Void, Void,Void> {
		ArrayList<NameValuePair> nvpAL = null;
		String cmd="";
		
		public bgSendCommandTask(String _cmd,ArrayList<NameValuePair> _nvpAL) {
			nvpAL = _nvpAL;
			cmd = _cmd;			
		}
		
		
		@Override
	    protected String doInBackground() {
			HttpClient httpClient = new DefaultHttpClient();
			String paramString;
			if (nvpAL == null) 
				paramString = "";
			else 
				paramString = URLEncodedUtils.format(nvpAL,"utf-8");
			
			String url = hostName+cmd+"?"+paramString;
			
			Log.e("url",url);
			
			HttpGet req = new HttpGet(url);	    	   
		    String resString = "xx";
			BufferedReader in = null;
		    
		    try {
		    	HttpResponse res = httpClient.execute(req);
		    	in = new BufferedReader(new InputStreamReader(
		                   res.getEntity().getContent()));
		    	resString = in.readLine();
		    } catch (Exception e) {
		    	Log.e("remoteCommand",e.getMessage());
		    }

			return resString;
	    }
	    
	}*/
}
