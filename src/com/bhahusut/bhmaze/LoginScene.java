package com.bhahusut.bhmaze;

import java.util.LinkedList;
import java.util.List;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class LoginScene extends BaseScene {
	
	public Font loginF;
	BitmapTextureAtlas boxT,loginBTT;
	ITextureRegion boxTR,loginBTTR;
	TiledTextureRegion boxTTR;
	InputText unameIT,passwordIT;
	
	
	LoginScene(Engine _eg, MainActivity _act, VertexBufferObjectManager _vbo,
			Camera _camera) {
		super(_eg, _act, _vbo, _camera);
		// TODO Auto-generated constructor stub
		this.sceneType = SceneManager.SceneType.SCENE_MAIN;
		this.createScene();
		
	}

	@Override
	public void loadGFX() {
		// TODO Auto-generated method stub
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		
		/*boxT = new BitmapTextureAtlas(eg.getTextureManager(), 300, 50,
				TextureOptions.DEFAULT);*/
		/*boxTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				boxT, act, "box.png", 0, 0);*/
		
		boxT = new BitmapTextureAtlas(eg.getTextureManager(),
				300, 50, TextureOptions.BILINEAR);						
		boxTTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						boxT, act, "box.png",0,0,1,1);
		boxT.load();		
		
		loginBTT = new BitmapTextureAtlas(eg.getTextureManager(),
				200, 50, TextureOptions.BILINEAR);
		loginBTTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						loginBTT, act, "loginBT.png",0,0,1,1);
		loginBTT.load();
		
	}

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScene() {
		Log.e("LoginScene","creating scene...");
		
		loadGFX();
		loadFonts();
		
		Text loginT = new Text(500, 10, loginF, "��͡�Թ"
				, vbo);
		this.attachChild(loginT);
		
		Text labelUnameT = new Text(300, 200, loginF, "���ͼ����"
				, vbo); 
		this.attachChild(labelUnameT);
		Text labelPwdT = new Text(300, 300, loginF, "���ʼ�ҹ"
				, vbo); 
		this.attachChild(labelPwdT);
		
		
		unameIT = new InputText(500, 200, "User name", "Enter username", boxTTR, 
				loginF, 17, 5, this.vbo, act);		
		this.attachChild(unameIT);
		this.registerTouchArea(unameIT);
		unameIT.setText("nol");
		
		passwordIT = new InputText(500, 300, "Password", "Enter password", boxTTR, 
				loginF, 17, 10, this.vbo, act);
		passwordIT.setPassword(true);
		this.attachChild(passwordIT);
		this.registerTouchArea(passwordIT);
		passwordIT.setText("lingnoy");				
		
		Sprite loginBTS = new Sprite(500,500,loginBTTR,vbo) {
			@Override
			public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, 
					final float pTouchAreaLocalX, final float pTouchAreaLocalY) {				
		         if (pSceneTouchEvent.isActionUp()) {
		        	List<NameValuePair> nvpl = new LinkedList<NameValuePair>();
		     		nvpl.add(new BasicNameValuePair("name",unameIT.getText()));
		     		nvpl.add(new BasicNameValuePair("password",passwordIT.getText()));
		     		String res = act.rc.sendCommand("login", nvpl);
		     		Log.e("login",res);
		     		
		         }
		         return true;
			}
		};
		this.registerTouchArea(loginBTS);						
		this.attachChild(loginBTS);
				
	}
	
	public void loadFonts() {
		FontFactory.setAssetBasePath("font/");
        final ITexture fontTexture = new BitmapTextureAtlas(
					act.getTextureManager(), 1024, 1024,
					TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		loginF = FontFactory.createFromAsset(act.getFontManager(), fontTexture,
				act.getAssets(), "TAHOMA.TTF", 40, true,
				Color.GREEN.getARGBPackedInt());
		loginF.load();
	}
	
}
