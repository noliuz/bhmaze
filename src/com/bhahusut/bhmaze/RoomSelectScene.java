package com.bhahusut.bhmaze;

import java.util.LinkedList;
import java.util.List;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class RoomSelectScene extends BaseScene {

	BitmapTextureAtlas rRoseT,rChabaT,rGluaymaiT;
	ITextureRegion rRoseTR,rChabaTR,rGluaymaiTR;
	Text rRoseTx,rChabaTx,rGluaymaiTx;
	final int maxRoomCount = 100;
	int rRoseCount=0,rChabaCount=0,rGluaymaiCount=0;
	RoomSelectScene sc;
	
	
	RoomSelectScene(Engine _eg, MainActivity _act,
			VertexBufferObjectManager _vbo, Camera _camera) {
		super(_eg, _act, _vbo, _camera);
		// TODO Auto-generated constructor stub
		sc = this;
		
		this.sceneType = SceneManager.SceneType.SCENE_MAIN;
		this.createScene();
	}

	@Override
	public void createScene() {
		// TODO Auto-generated method stub
		Log.e("RoomSelectScene","creating scene...");
		
		this.loadGFX();
		
		Sprite roseS = new Sprite(500,150,rRoseTR,vbo) {
			@Override
			public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {			       
				if (pSceneTouchEvent.isActionUp()) {
					Log.e("xx",joinRoom("����Һ"));
				}					
				return true;
			}

		};
		Sprite chabaS = new Sprite(500,200,rChabaTR,vbo);
		Sprite gluaymaiS = new Sprite(500,250,rGluaymaiTR,vbo);
		
		this.registerTouchArea(roseS);
		this.attachChild(roseS);
		
		
		this.attachChild(chabaS);
		this.attachChild(gluaymaiS);
		
		updateRoomCount();		
		
	}

	public String joinRoom(String rname) {
		List<NameValuePair> nvpl = new LinkedList<NameValuePair>();
 		nvpl.add(new BasicNameValuePair("sId",
 				"1176e14055cb0fc5e2225fb9f6c2b4d3"));
 		nvpl.add(new BasicNameValuePair("rname",rname));
 		String res = act.rc.sendCommand("join_room", nvpl);
				
		return res;
	}
	
	public void updateRoomCount() {
		String res = act.rc.sendCommand("get_rooms_joined_count", null);
		Log.e("xx",res);
	}
	
	@Override
	public void loadGFX() {
		// TODO Auto-generated method stub
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		
		rRoseT = new BitmapTextureAtlas(eg.getTextureManager(),
				200, 40, TextureOptions.BILINEAR);						
		rRoseTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						rRoseT, act, "room_rose.png",0,0,1,1);
		rRoseT.load();
		
		rChabaT = new BitmapTextureAtlas(eg.getTextureManager(),
				200, 40, TextureOptions.BILINEAR);						
		rChabaTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						rChabaT, act, "room_chaba.png",0,0,1,1);
		rChabaT.load();
		
		rGluaymaiT = new BitmapTextureAtlas(eg.getTextureManager(),
				200, 40, TextureOptions.BILINEAR);						
		rGluaymaiTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						rGluaymaiT, act, "room_gluaymai.png",0,0,1,1);
		rGluaymaiT.load();
		
	}

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}
	
}
