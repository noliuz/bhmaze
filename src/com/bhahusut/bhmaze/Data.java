package com.bhahusut.bhmaze;

import java.util.ArrayList;

import org.andengine.entity.sprite.Sprite;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class Data {
	public static class Mak {
		public int x,y;
		public Sprite sp;
		public char p;
		
		Mak(int _x,int _y,Sprite _sp,char _p) {
			x = _x;
			y = _y;
			sp = _sp;
			p = _p;
		}
		
		public boolean posEquals(int _x,int _y) {
			if (_x == x && _y == y)
				return true;
			else
				return false;
		}
	}
	
	public static class Point {
		public int x;
		public int y;
		
		public Point(int _x,int _y) {
			x = _x;
			y = _y;
		}
	}
	
	public static boolean isIn(ArrayList<Mak> mdal,
			int x,int y) {
		for (int i=0;i<mdal.size();i++) {
			if (mdal.get(i).x == x && mdal.get(i).y == y) {
				return true;
			}
		}
		
		return false;
	}
	
	public static int indexOf(ArrayList<Mak> mal,
			int x,int y) {		
		for (int i=0;i<mal.size();i++) {
			if (mal.get(i).posEquals(x,y)) {
				return i;
			}
		}
		
		return -1;
	}
	
	public static class resCommand {
		String command;
		ArrayList<NameValuePair> paramsNVP = new ArrayList<NameValuePair>();
		
		public void addParam(String key,String val) {
			paramsNVP.add(new BasicNameValuePair(key,val));
		}
		
		public String getVal(String key) {
			for (int i=0;i<paramsNVP.size();i++) {
				String ktmp = paramsNVP.get(i).getName();
				if (ktmp.equals(key)) {
					return paramsNVP.get(i).getValue();
				}
			}
			return "null";
		}
	}
}
