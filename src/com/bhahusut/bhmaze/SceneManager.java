package com.bhahusut.bhmaze;

import org.andengine.engine.Engine;

public class SceneManager {

	private BaseScene mainScene;
	
	public static final SceneManager INSTANCE = new SceneManager();
	private SceneType currentSceneType = SceneType.SCENE_MAIN;
	private BaseScene currentScene;
	private Engine eg;
	public enum SceneType
    {
        SCENE_MAIN,        
    }
	
	public void setScene(BaseScene scene)
    {
        eg.setScene(scene);
        currentScene = scene;
        currentSceneType = scene.sceneType;
    }
	
	public void setScene(SceneType sceneType)
    {
        switch (sceneType)
        {
            case SCENE_MAIN:
                setScene(mainScene);
                break;
        }
    }	
	
}
